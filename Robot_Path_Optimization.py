
# coding: utf-8

# # Robot Path Finding
# 
# In this problem, we will use optimization to help this robot find the most
# efficient path between two given points.
# There is a catch though - someone (without a heart, obviously)  put some obstacles in its way.
# In this question, your task is to help the robot find the most efficient path between two points,
# while avoiding all those obstacles!

# You are given an initial path matrix, $X$, comprised of $n+1$ vectors (each with 2 coordinates)
# $\vec x_0,\vec x_1, \vec x_2, \vec x_3...\vec x_n$.
# The start and end points of your path are fixed, and are given by $\vec x_0$ and $\vec x_n$. 
# 
# You are also given certain point obstacles (2-vectors), $\vec r_i$, in the
# obstacle matrix $R$. In order to optimize the robot`s path, you will be using
# the **_Steepest Descent_** algorithm.
# 
# The objective function you will be using in order to carry out the optimization
# is given by $F(\vec x_0,\vec x_1....\vec x_n)$  defined as 
# 
# $$F(\vec x_0,\vec x_1....\vec x_n) =
# c_1 \sum_{i=0}^{n} \sum_{j=1}^{k}\frac{1}{\epsilon + \|\vec x_i - \vec r_j\|^2_2}
# + c_2 \sum_{i=0}^{n-1}\|\vec x_{i+1} - \vec x_i\|^2_2 $$
# 
# The symbols in this formula mean the following:
# 
# *   $c_1$ and $c_2$ are "weights" (constants) that determine the effect of each term on the value of $F(x_i)$.
# 
#     *   $c_1$ controls the 'loss' the objective function suffers for getting close to the obstacles.
#     *   $c_2$ controls the 'loss' the objective function suffers for having very long line segments.
# 
# *   $k$ is the number of obstacles your robot must avoid.
# 
# *   Adding $\epsilon$ in the denominator prevents division by zero

# In order to understand the objective function, look at each term individually:
# 
# $$F(\vec x_0,\vec x_1,...,\vec x_n) = A + B$$
# 
# * The algorithm mainly tries to minimize the value of the objective function.
# Term $A$, also called the _Penalty term_, uses the distance of each point $x_i$
# from each obstacle $r_j$. The division sign ensures that the farther the distance
# of a point from the obstacle, the smaller the value of A, therefore reducing the overall value of $F$.
# 
# * Term $B$ uses the distance of each point $x_i$ from the next corresponding point $x_{i+1}$.
# Since $B$ will always be non-negative, the minimum value it can attain is zero,
# which further implies that the minimum is achieved when the distance between
# points $x_i$ and $x_i+1$ is as small as possible, which in turn happens when
# all line segments are about equally long.

# # Robot Path Finding: Implementing the Gradient
# 
# Implement the gradient formula of the objective function you have derived in the previous problem.
# 
# Your code will receive the following data:
# 
# *   `P`, a $m \times n \times 2$ path matrix where there are $m$ paths,
#     each path containing $n$ points in the path, including the start and end points.
# 
#     So for a given `i`, `P[i]` is $n\times 2$ and corresponds to one path from the
#     previous problem.
# 
# *   `obstacles`, a $k \times 2$ matrix indicating the coordinates of the obstacles.
# *   `c1`, the value $c_1$ in the objective function.
# *   `c2`, the value $c_2$ in the objective function.
# *   `eps`, the value $\epsilon$ in the objective function.
# 
# Your code should compute the following quantities:
# 
# *   `G`, a $m \times n \times 2$ matrix indicating the gradient for every path in `P`.
# 
# **Notes:**
# 
# *   Although, mathematically speaking, $F$ does have a non-zero gradient with respect to the start and end points,
#     we actually do not want the steepest descent algorithm to move those two points. As a hack to accomplish this,
#     we set their gradients to 0.
# 
# *   To help debug your gradient, you can try the following:
# 
#     *   Implement the objective function as `obj`
# 
#     *   Make up a random path `p` and a random offset `offs`, both of shape $n\times 2$
# 
#     *   `obj(p+offs) - obj(p)` and `dobj(p).reshape(-1).dot(offs.reshape(-1))` should
#         agree with each other better and better, the smaller `offs` gets.
# 
#         (Why? Hint: Taylor's theorem.)
# 
#     *   You can now disable parts of your objective function and test parts of the gradient.

# In[10]:

import numpy as np
import numpy.random as ra

P = ra.rand(10, 21, 2) * 20
obstacles = ra.rand(5, 2) * 20

c1 = 10
c2 = 1
eps = 0.1


# class Ref:
#     def __init__(self):
#         ###CORRECT_CODE###

#         self.G = G

# ref = Ref()
# feedback.check_numpy_array_allclose("G", ref.G, G)
# feedback.finish(1, "Everything seems good!")


import numpy.linalg as la

def obj(x, print_components=False):
    x = x.astype(np.float64)
    obstacle_vecs = x.reshape(-1, 1, 2) - obstacles

    obstacle_sq_dists = np.sum(obstacle_vecs**2, axis=-1)
    point_dists = np.sum(np.diff(x, axis=0)**2)

    avoidance_comp = c1 * np.sum(1/(eps + obstacle_sq_dists))
    spacing_comp = c2 * point_dists

    if print_components:
        print(avoidance_comp, spacing_comp)
    return avoidance_comp + spacing_comp

def dobj(x):
    x = x.astype(np.float64)

    seg_vecs = np.diff(x, axis=0)

    npoints, _ = x.shape

    # (npoints, nobstacles, 2)
    obstacle_vecs = x.reshape(-1, 1, 2) - obstacles
    obstacle_sq_dists = np.sum(obstacle_vecs**2, axis=-1)

    result = np.zeros_like(x)
    result += 2*c1*np.sum(
            - obstacle_vecs
            /(eps+obstacle_sq_dists.reshape(npoints, -1, 1))**2, axis=1)

    result[1:] += 2*c2*seg_vecs
    result[:-1] += -2*c2*seg_vecs

    result[0] = 0
    result[-1] = 0

    return result

if 0:
    # Test that dobj is the gradient of obj
    cur_path = np.random.randn(*init_path.shape)

    h_mag = 1e-7
    h = h_mag*np.random.randn(*init_path.shape)
    # Not moving first and last point
    h[0] = 0
    h[-1] = 0

    diff =  obj(cur_path+h) - obj(cur_path)
    pred_diff = dobj(cur_path).reshape(-1).dot(h.reshape(-1))

    # grad error should be proportional to h_mag
    print("Grad error:", abs(diff-pred_diff)/abs(diff))

m = P.shape[0]
G = np.zeros(P.shape)
for i in range(m):
    G[i] = dobj(P[i])


# # Robot Path Finding: Putting it all together
# 
#     ### Let's get the robot to move!
# 
#     **NOTE:** This problem is independent of the previous one. You do not need any
#     results from that problem to start this one.
# 
#     In this question, you will be given a initial path, and you will be asked
#     to optimize it according to our objective function. Do the following:
# 
#     *   Run the optimization for 500 iterations.
#     *   In every iteration, compute the gradient value and then use golden search to find
#         the optimal step size. Use `scipy.optimize.golden`.
#     *   Plot the initial path, the path after 10, 20, 30, 50, 100, and 200 iterations,
#         and the final path,
#         all in the same plot, with labels indicating which path is which. Also plot the
#         obstacles from the `obstacles` array.
# 

# In[15]:

import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
import scipy.optimize as sopt

obstacles = np.array([10, 0]) + 10*np.random.randn(30, 2)

init_path = np.array([[0, 0], [1, 1], [2, 2], [3, 9], [4, 9], [5, 20],
    [6, 20], [7, 20], [8, 25], [9, 20], [10, -20],[11, -20], [12, -16], [13, -16],
    [14, -18], [15, -17], [16, -30], [17, -20], [18, -15], [19, -10], [20, 0]],
    dtype=np.float64)

c1 = 10
c2 = 1
eps = 0.1

def obj(x, print_components=False):
    x = x.astype(np.float64)
    obstacle_vecs = x.reshape(-1, 1, 2) - obstacles

    obstacle_sq_dists = np.sum(obstacle_vecs**2, axis=-1)
    point_dists = np.sum(np.diff(x, axis=0)**2)

    avoidance_comp = c1 * np.sum(1/(eps + obstacle_sq_dists))
    spacing_comp = c2 * point_dists

    if print_components:
        print(avoidance_comp, spacing_comp)
    return avoidance_comp + spacing_comp

def dobj(x):
    x = x.astype(np.float64)

    seg_vecs = np.diff(x, axis=0)

    npoints, _ = x.shape

    # (npoints, nobstacles, 2)
    obstacle_vecs = x.reshape(-1, 1, 2) - obstacles
    obstacle_sq_dists = np.sum(obstacle_vecs**2, axis=-1)

    result = np.zeros_like(x)
    result += 2*c1*np.sum(
            - obstacle_vecs
            /(eps+obstacle_sq_dists.reshape(npoints, -1, 1))**2, axis=1)

    result[1:] += 2*c2*seg_vecs
    result[:-1] += -2*c2*seg_vecs

    result[0] = 0
    result[-1] = 0

    return result


import matplotlib.pyplot as pt

def obj1d(alpha):
        return obj(x + alpha*s)

x = init_path

pt.figure(figsize=(8,8))
pt.plot(x[:,0], x[:,1], label="Initial")
cnt = 0

for cnt in range(500):
    s = -dobj(x)

    alpha_opt = sopt.golden(obj1d)
    xnext = x + alpha_opt * s

    x = xnext

    if cnt in [10, 20, 30, 50, 100, 200]:
        pt.plot(x[:,0], x[:,1], label="After %d it" % cnt)

final_path = x
get_ipython().magic('matplotlib inline')


pt.plot(x[:,0], x[:,1], label="Final")
pt.plot(obstacles[:,0], obstacles[:,1], "o", label="Obstacles")
pt.legend(loc="best")

print("The goal is to minimize our path length and the "
      "initial path is quite inefficient in this regard."
      " We observe that as our algorithm iterates, the length of the path "
      "is reduced. It would form a straight line if only the path length was to be minimized, "
      "but we can see from the results that the final path also curves away from our obstacles."
      " This behavior is due to the penalty terms in our objective function. "
      " Therefore we get a final (500 iterations) path that is trade off between avoiding the obstacles "
      "and creating the shortest length."
      " Also of note, is the slow convergence after the 100th or 200th iteration as "
      "the final path is approached.")


# In[ ]:



