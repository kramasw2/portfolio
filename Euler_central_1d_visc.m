
function dqdt = Euler_central_1d_visc(t,q,Nx,D, D2, nu, gam)
dqdt = zeros(size(q));
rho = q(1:Nx,1);
rhou = q((Nx+1):2*Nx,1);
u = rhou./rho;
rhoE = q((2*Nx+1):3*Nx,1);
E = rhoE./rho;
e = (rhoE - 0.5*rho.*(u.^2))./rho;
p = (gam-1).*(rho.*e);

% Central diff stuff
dudt = -D*(rhou) + nu*D2*(rho);
drudt = -D*(rhou.*u + p) + nu*D2*(rhou);
drEdt = -D*(rhoE.*u + p.*u) + nu*D2*(rhoE);

dqdt(1:Nx,1) =    dudt;
dqdt(Nx+1:2*Nx,1) =   drudt;
dqdt(2*Nx+1:3*Nx,1) = drEdt;

end
