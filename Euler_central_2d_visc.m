function dqdt = Euler_central_2d_visc(t, q, Nx, Ny, D, D2, nu, gam)
dqdt = zeros(size(q));
rho = q(1:Nx*Ny,1);
rho = reshape(rho, Nx, Ny);

rhou = q((Nx*Ny+1):2*Nx*Ny,1);
rhou = reshape(rhou, Nx,Ny);
u = rhou./rho;

rhov = q((2*Nx*Ny+1):3*Nx*Ny,1);
rhov = reshape(rhov, Nx,Ny);
v = rhov./rho;

rhoE = q((3*Nx*Ny+1):4*Nx*Ny,1);
rhoE = reshape(rhoE, Nx, Ny);
E = rhoE./rho;

e = (rhoE - 0.5*rho.*(u.^2) - 0.5*rho.*(v.^2))./rho;
p = (gam-1).*(rho.*e);


% Central diff stuff

drhodt = -D*rhou - rhov*D' + nu.*(D2*rho + rho*D2');  
drhoudt = -D*(rhou.*u + p) - (rhov.*u)*D' + nu.*(D2*rhou + rhou*D2');  
drhovdt = -D*(rhou.*v) - (rhov.*v+p)*D' + nu.*(D2*rhov + rhov*D2');
drhoEdt = -D*(u.*(rhoE+p)) - (v.*(rhoE+p))*D' +nu.*(D2*rhoE + rhoE*D2');

dqdt(1:Nx*Ny,1) =   reshape(drhodt, Nx*Ny,1);
dqdt(Nx*Ny+1:2*Nx*Ny,1) =   reshape(drhoudt, Nx*Ny,1);
dqdt(2*Nx*Ny+1:3*Nx*Ny,1) = reshape(drhovdt, Nx*Ny,1);
dqdt(3*Nx*Ny+1:4*Nx*Ny,1) = reshape(drhoEdt, Nx*Ny,1);

end
