function F = roeflux(qL, qR, gam)

rhoL = qL(1);
rhoR = qR(1);
uL = qL(2)/qL(1);
uR = qR(2)/qR(1);
EL = qL(3)/qL(1);
ER = qR(3)/qR(1);
pL = (gam - 1)*(qL(3) - 0.5*rhoL*uL*uL);
pR = (gam - 1)*(qR(3) - 0.5*rhoR*uR*uR);

% Compute u_hat, h_hat, c_hat, Lambda_hat, X_hat
hL = EL - 0.5*((uL)^2) + pL/rhoL;
hR = ER - 0.5*((uR)^2) + pR/rhoR;

u_hat = (sqrt(rhoL)*uL + sqrt(rhoR)*uR)/(sqrt(rhoL) + sqrt(rhoR));
h_hat = (sqrt(rhoL)*hL + sqrt(rhoR)*hR)/(sqrt(rhoL) + sqrt(rhoR));
c_hat = abs(sqrt((gam-1)*(h_hat - 0.5*u_hat*u_hat)));
rho_hat = sqrt(rhoL*rhoR);

lambda_hat = [u_hat u_hat-c_hat u_hat+c_hat]';
X_hat = zeros(3,3);
X_hat(:,1) = [1 u_hat       0.5*u_hat*u_hat]';
X_hat(:,2) = [1 u_hat-c_hat h_hat-u_hat*c_hat]';
X_hat(:,3) = [1 u_hat+c_hat h_hat+u_hat*c_hat]';

% Find wjump
wjump = zeros(3,1);
wjump(1) = (rhoR - rhoL) - (pR - pL)/(c_hat*c_hat);
wjump(2) = 0.5*((pR-pL) - c_hat*rho_hat*(uR-uL))/(c_hat*c_hat);
wjump(3) = 0.5*((pR-pL) + c_hat*rho_hat*(uR-uL))/(c_hat*c_hat);

% Fudge factor - Entropy Fix
xi = 0.01;
if abs(lambda_hat(2)) < xi
    lambda_hat(2) = 0.5*((lambda_hat(2)^2)/xi + xi);
elseif abs(lambda_hat(3)) < xi
    lambda_hat(3) = 0.5*((lambda_hat(3)^2)/xi + xi);
end

% Calculate fqL, fqR, F
fq = @(q, rho, u, E, p) [rho*u rho*u*u+p (rho*E+p)*u]';

fqL = fq(qL, rhoL,uL,EL,pL);
fqR = fq(qR, rhoR,uR,ER,pR);

F = 0.5*(fqL + fqR) - 0.5*((abs(lambda_hat(1))*wjump(1)).*X_hat(:,1) + ...
                           (abs(lambda_hat(2))*wjump(2)).*X_hat(:,2) + ...
                           (abs(lambda_hat(3))*wjump(3)).*X_hat(:,3));
end
