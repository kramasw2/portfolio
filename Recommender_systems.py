
# coding: utf-8

# # Recommender System for Movies
# 
# ## Introduction
# 
# A recommendation system is one that you might have unknowingly encountered while using websites such as Pandora Radio or Netflix. 
# The basic idea behind a recommendation system is to "recommend" content to users based on their current preferences. These current 
# preferences for a specific user are gauged based on the users ratings on previous content accessed on the website.
# 
# In this question, you will be implementing a recommender system capable of suggesting movies for users (ones that they _have not_ yet watched),
# using information about their preferences (ratings on the movies that they _have_ already watched).
# You will be given an incomplete matrix `ratings`, containing ratings provided by 25 users for 100 movies. 
# 
# In case the user has not yet seen the movie, there is no rating specified. This will be indicated by a `NaN` (Not a number) 
# value at that location.
# 
# Each user is identified by an index, given to you in a numpy array, `userattr`, whereas each movie is given in `movieattr` as a string (numpy.str_). 
# Note that the indices of `userattr` and `movieattr` correspond to the row and column indices of the ratings matrix, respectively.
# Note that each element in `movieattr` is a string, giving you the name of the movie. Each user, however, is identified by a user ID of type int.

# ## How do I go about solving this?
# 
# You will be using SVD to implement your recommender system. However, Using full matrices (U,S,V) "over-fits" the data and does not 
# represent a true prediction. A better prediction may be obtained by using **significant** 'features' ($\sigma$ values) of the data 
# in order to recostruct the complete recommendation matrix. The remaining features can therefore be construed as 'noise'.
# We need to also minimize the Root Mean Square Error (RMSE) of this prediction model, for which the 'best rank-k' 
# approximation of the SVD is carried out, since (considering any general matrix A):
# 
# $$\|A - A_k\|_F = min(\sqrt{\sum(a_{ij} - a_{ij}')^2)})$$

# 1. However, carrying out an SVD on an incomplete matrix is not possible. Therefore, prior to performing the SVD, an 'initial guess' for 
#      each unknown rating may be estimated as the average of the **known** ratings for that movie (along the column). 
# 
#   1. After you "fill in" the `ratings` matrix, you need to normalize the matrix (using **user** averages), as follows
# 
#     * Remove the user mean:
#     $$\widetilde{\mathbf r}_{i} :=\mathbf r_{i}-\mathbf{u_i},\qquad  i=1,\dots,N.$$
# 
#     * Normalized: $$\widetilde{\mathbf R} := \frac1{\sqrt{N-1}}
#                       \begin{bmatrix}
#                         \widetilde{\mathbf r}_1 &
#                         \widetilde{\mathbf r}_2 &
#                         \cdots &
#                         \widetilde{\mathbf r}_N
#                       \end{bmatrix}$$
# 
# 
# 
#   1. Now, perfrom an SVD of the normalized `ratings_matrix`. 
#      You will now have to reconstruct your original matrix by carrying out a 'best rank-15' approximation. 
#      Remember, you would need to only take into account the 15 most signicant features.
# 
#     $$A = \sum_{i=1}^{15} (\sigma_i \mathbf{u_i v_i^T})$$
# 
#   1. Calculate the RMSE of your prediction with respect to your original matrix:
# 
#     $$RMSE = \sqrt{\sum(r_{ij} - a_{ij})^2)}/m$$
# 
#     * $r_{ij}$ corresponds to a known value in the original matrix
#     * $a_{ij}$ is a corresponding element in the prediction matrix.
#     * $m$ is the number of known values in the original matrix
# 
#   1. You have now predicted user ratings that were previously unavailable! However, in the process, you have also 
#      overwritten the ratings that were previously available. You will therefore need to revert these to  their original values.
# 
#   1. Now that you know the recommended movies for each user, you will be given a User ID in `userid`, for whom you need to provide 3
#      movie recommendations in `movierec`, a numpy array containing 3 recommendations of type (numpy.str_). Make sure to arrange the contents of `movierec`
#      in descending order of their ratings.
# 

# If you successfully solve this question, you will be a step closer to competing for the
# "[Netflix Prize](http://en.wikipedia.org/wiki/Netflix_Prize)"!

# ##Setup Code##

# In[30]:

import numpy as np
from io import StringIO
import csv

#Ratings data as users x movies
# data_files = {}
#  = "data/ratingsdata.csv"

# for file in data_files.values():
#     f = StringIO(file.decode("utf-8"))
with open('data/ratingsdata.csv', 'r') as csvfile:
    r = csv.reader(csvfile)
    data = np.asarray(list(r))
userattr = data[1:,0].astype(int)
movieattr = data[0,1:]
ratings = data[1:,1:]

#NaN values to missing data
ratings[ratings==''] = np.nan
ratings = ratings.astype(float)

#Randomize Ratings
ratings = np.random.permutation(ratings.T)

#Random user generator
userid = np.random.choice(userattr)


# ##Answer Code##

# In[36]:

import numpy.linalg as la


# Fill up movie ratings
movie_avg = np.nanmean(ratings, axis = 0)
nan_ratings = np.isnan(ratings)
usr,mov = ratings.shape

prediction = np.copy(ratings)
prediction[nan_ratings] = np.tile(movie_avg,(usr,1))[nan_ratings]


# Normalize ratings matrix wrt user averages
user_avg = np.mean(prediction, axis = 1)
prediction -= user_avg[:,None]
prediction /= np.sqrt(mov - 1)

# Perform SVD, Rank k approximations
U,S,VT = la.svd(prediction,full_matrices = False)

k = 15
S_k = np.diag(S[:k])
U_k = U[:,:k]
VT_k = VT[:k]

# Prediction by remultiplying SVD and reverting normalization
prediction = np.sqrt(mov - 1)*U_k.dot(S_k.dot(VT_k))
prediction += user_avg[:,None]

# Calculate RMSE
Err = prediction[~nan_ratings] - ratings[~nan_ratings]
m = np.count_nonzero(~nan_ratings)
RMSE = la.norm(Err,2)/np.sqrt(m)

# Replace old known ratings back into the new matrix
prediction[~nan_ratings] = ratings[~nan_ratings]

# Print top 3 recommendations for userID
user_old_ratings = ratings[np.where(userattr == userid)]
user_new_ratings = prediction[np.where(userattr == userid)]
nan_user = np.isnan(user_old_ratings)

recindex = np.argsort(user_new_ratings[nan_user])[::-1]
movierec = movieattr[recindex[:3]]

print(RMSE)
print("User: ",userid)
print("Recommendations for User #", userid ,": ", movierec)


# In[ ]:



