clear all
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Final_project - Karthik Ramaswamy
%	2D Euler - Shock Bubble Problem
%	
%		Q_t + F(Q)_x = 0
%
%       where q = [Q1 Q2 Q3]' = [ rho rho*u rho*E]'
%             E = e + 0.5*(u.^2);
%             F(Q) = [F1 F2 F3]' = [rho*u rho*u*u+p (rho*E + p)*u]';
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Default plot attributes
	set(0,'defaultfigurecolor',		[1 1 1]);
	set(0,'defaultfigureposition',	[10 300 800 550]);
    
fprintf('********************************************************* \n');
fprintf('2D Euler Solver for the Shock Bubble problem \n');
fprintf('Author: Karthik Ramaswamy \n');
fprintf('********************************************************* \n');

% Test inputs

Lx = 1.0;
Ly = 1.0;
Nx = 400;
Ny = 400;
PeriodicFlagx = 0;
T = 0.2;
Nt = 100;

% construct mesh, time step
[y,x] = meshgrid(0:Nx-1,0:Ny-1);
y = y./(Ny);
x = x./(Nx);
dx = x(2,1) - x(1,1);
dy = y(1,2) - y(1,1);

% eps = 0.25;
nux = dx/2;
dt = T/Nt;
gam = 1.4;

t_plot = [0:dt:T];

% % construct periodic FD matrix
D   = sparse(central_2nd_order(x,PeriodicFlagx));
D2 = sparse(central_2nd_order_second_derivative(x, PeriodicFlagx));

% Initial Conditions
rho(1:0.1*Nx,1:Ny) = 3.81;
rho(0.1*Nx+1:Nx,1:Ny) = 1.0;
u(1:0.1*Nx,1:Ny) = 2.85;
u(0.1*Nx+1:Nx,1:Ny) = 0;
v(1:0.1*Nx,1:Ny) = 0;
v(0.1*Nx+1:Nx,1:Ny) = 0;
p(1:0.1*Nx,1:Ny) = 10;
p(0.1*Nx+1:Nx,1:Ny) = 1;

% Set up bubble
for i = 1:Nx
    for j = 1:Ny
        if ((x(i,j) - 0.35)^2 + (y(i,j) - 0.5)^2) <= 0.04
                rho(i,j) = 0.1;
                u(i,j) = 0;
                v(i,j) = 0;
                p(i,j) = 1.0;
         end
     end
end             
    
e = p./((gam - 1.0).*rho);

% h = pcolor(x,y,e);
% xlim([0 1]); ylim([0 1]);xlabel('x'); ylabel('y');
% axis equal
% set(h,'edgecolor','none')

Q0 = zeros(4*Nx*Ny,1);
Q0(1:Nx*Ny,1) = reshape(rho,Nx*Ny,1);
Q0(Nx*Ny+1:2*Nx*Ny,1) = reshape(rho.*u,Nx*Ny,1);
Q0(2*Nx*Ny+1:3*Nx*Ny,1) = reshape(rho.*v,Nx*Ny,1);
Q0(3*Nx*Ny+1:4*Nx*Ny,1) = reshape(rho.*e + 0.5.*(rho.*u.*u + rho.*v.*v),Nx*Ny,1);

% Time integration using both Average and Reimann integration methods
[t,Q] = ode45(@(t,Q)   Euler_central_2d_visc(t,Q, Nx, Ny, D, D2, nux, gam),t_plot,Q0);
 
video = VideoWriter('Bubble.avi', 'Uncompressed AVI');
open(video);
for n = 1:length(t)
    
%     n = 51;
    rho = Q(n,1:Nx*Ny);
    rho = reshape(rho, Nx, Ny);

    rhou = Q(n,(Nx*Ny+1):2*Nx*Ny);
    rhou = reshape(rhou, Nx,Ny);
    u = rhou./rho;

    rhov = Q(n,(2*Nx*Ny+1):3*Nx*Ny);
    rhov = reshape(rhov, Nx, Ny);
    v = rhov./rho;

    rhoE = Q(n,(3*Nx*Ny+1):4*Nx*Ny);
    rhoE = reshape(rhoE, Nx, Ny);
    E = rhoE./rho;

    e = (rhoE - 0.5*rho.*(u.^2) - 0.5*rho.*(v.^2))./rho;
    p = (gam-1).*(rho.*e);
        
    vort = u*D' - D*v;
    
    % plotting
    subplot(2,3,1);
    h1 = pcolor(x,y,u);
    xlim([0 1]); ylim([0 1]);xlabel('x - u contours'); ylabel('y');
    axis equal
    set(h1,'edgecolor','none')

    subplot(2,3,2);
    h2 = pcolor(x,y,v);
    xlim([0 1]); ylim([0 1]);xlabel('x - v contours'); ylabel('y');
    axis equal
    set(h2,'edgecolor','none')
    
    title(['Solution parameters at time t = ',num2str(t(n)),'s']);
    subplot(2,3,3);
    h3 = pcolor(x,y,rho);
    xlim([0 1]); ylim([0 1]);xlabel('x - \rho contours'); ylabel('y');
    axis equal
    set(h3,'edgecolor','none');

    subplot(2,3,4);
    h4 = pcolor(x,y,p);
    xlim([0 1]); ylim([0 1]);xlabel('x - p contours'); ylabel('y');
    axis equal
    set(h4,'edgecolor','none')

    subplot(2,3,5);
    h5 = pcolor(x,y,e);
    xlim([0 1]); ylim([0 1]);xlabel('x - e contours'); ylabel('y');
    axis equal
    set(h5,'edgecolor','none');

    subplot(2,3,6);
    h6 = pcolor(x,y,vort);
    xlim([0 1]); ylim([0 1]);xlabel('x - \omega contours'); ylabel('y');
    axis equal
    set(h6,'edgecolor','none');
    drawnow;
    FF = getframe(gcf);
    writeVideo(video,FF);
end

close(video);

