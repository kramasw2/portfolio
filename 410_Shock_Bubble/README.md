
##Matlab:##
* Code for a Shock Bubble Interaction Problem I wrote over the summer. The problem is currently set up to solve the 2D Euler Equations for the Shock Bubble interaction problem, although it can be used for other problems (preferably goverened by hyperbolic equations).
* The .m files included are functions used within the Main Shock_Bubble code.
* Roeflux.m is a roe flux computing function, that uses a Reimann solver instead of something like ODE45.
