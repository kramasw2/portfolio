function D = Disc_scheme(x,PeriodicFlag, sd)
if strcmp(sd,'C2') == 1
%-----------------------------
% CENTRAL_2nd_Order
Nx = max(size(x));
dx = x(2) - x(1);

V1 = ones([1,Nx-1]);
V2 = ones([1,Nx-2]);
V3 = ones([1,Nx-3]);

D = (-diag(V1,-1) + diag(V1,1))/(2*dx);


if(PeriodicFlag == 0)
	D(1,:)	= [-1 1 0*V2]/dx;
	D(Nx,:)	= [0*V2 -1 1]/dx;
elseif(PeriodicFlag == 1) 
	D(1,:)	= [0 1 0*V3 -1]/(2*dx);
	D(Nx,:)	= [1 0*V3 -1 0]/(2*dx);
else
	error('Incorrect PeriodicFlag');
end

%-----------------------------
% UPWIND_2nd_ORDER
elseif strcmp(sd,'U2') == 1
Nx = max(size(x));
dx = x(2) - x(1);

V0 = ones([1,Nx]);
V1 = ones([1,Nx-1]);
V2 = ones([1,Nx-2]);
V3 = ones([1,Nx-3]);
%V4 = ones([1,Nx-4]);

D = (diag(V2,-2) - 4*diag(V1,-1) + 3*diag(V0,0))/(2*dx);

if(PeriodicFlag == 0)
	D(1,:)  = [-1 1 0*V2]/(dx);
	D(2,:)  = [-1 1 0*V2]/(dx);
elseif(PeriodicFlag == 1) 
	D(1,:)		= [3 0*V3 1 -4]/(2*dx);
	D(2,:)		= [-4 3 0*V3 1]/(2*dx);
else
	error('Incorrect PeriodicFlag');
end

%-----------------------------
% UPWIND_3rd_ORDER
elseif strcmp(sd,'U3') == 1
Nx = max(size(x));
dx = x(2) - x(1);

V0 = ones([1,Nx]);
V1 = ones([1,Nx-1]);
V2 = ones([1,Nx-2]);
V3 = ones([1,Nx-3]);
V4 = ones([1,Nx-4]);

D = (diag(V2,-2) - 6*diag(V1,-1) + 3*diag(V0,0) + 2*diag(V1,+1))/(6*dx);

if(PeriodicFlag == 0)
  D(1,:)  = [-1 1 0*V2]/(dx);
  D(2,:)  = [-1 1 0*V2]/(dx);
  D(Nx,:) = [0*V2 -1 1]/(dx);
elseif(PeriodicFlag == 1) 
	D(1,:)		= [3 2 0*V4 1 -6]/(6*dx);
	D(2,:)		= [-6 3 2 0*V4 1]/(6*dx);
	D(Nx,:)		= [2 0*V4 1 -6 3]/(6*dx);
else
	error('Incorrect PeriodicFlag');
end
else
    error('Incorrect key/ discretization scheme');
end
    
end
