function dqdt = Euler_1d_FV(t,Q,Nx, h, nux, gam, sch)

    dqdt = zeros(size(Q));
    rho = Q(1:Nx,1);
    rhou = Q((Nx+1):2*Nx,1);
    u = rhou./rho;
    rhoE = Q((2*Nx+1):3*Nx,1);
    e = (rhoE - 0.5*rho.*(u.^2))./rho;
    % p = (e - 1/2*(u.^2)).*((gam - 1.0).*rho);
    p = (e).*((gam - 1.0).*rho);

    % Assign F1 F2 F3 for other two methods (not incl. Roe)
    F1 = rho.*u;
    F2 = rho.*(u.^2) + p;
    F3  = (rhoE + p).*u;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Roe's Method %%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(sch, 'Roe')
        % Find F_(i+1/2)
    for i=1:Nx-1
        
        qL = [rho(i,1) rhou(i,1) rhoE(i,1)]';
        qR = [rho(i+1,1) rhou(i+1,1) rhoE(i+1,1)]';
        
        F = roeflux(qL, qR, gam);
        
        F1_phalf(i) = F(1);
        F2_phalf(i) = F(2);
        F3_phalf(i) = F(3);
    end
    
    for i = 2:Nx
        qL = [rho(i-1,1) rhou(i-1,1) rhoE(i-1,1)]';
        qR = [rho(i,1) rhou(i,1) rhoE(i,1)]';
        
        F = roeflux(qL, qR, gam);
        
        F1_mhalf(i) = F(1);
        F2_mhalf(i) = F(2);
        F3_mhalf(i) = F(3);
    end
    
    % Boundary Conditions - F_(i + 1/2)
    qL = [rho(Nx,1) rhou(Nx,1) rhoE(Nx,1)]';
    qR = [rho(Nx,1) rhou(Nx,1) rhoE(Nx,1)]';
    
    F = roeflux(qL, qR, gam);
    F1_phalf(Nx) = F(1);
    F2_phalf(Nx) = F(2);
    F3_phalf(Nx) = F(3);

    % Boundary Conditions - F_(i - 1/2)    
    qL = [rho(1,1) rhou(1,1) rhoE(1,1)]';
    qR = [rho(1,1) rhou(1,1) rhoE(1,1)]';
    
    F = roeflux(qL, qR, gam);
    F1_mhalf(1) = F(1);
    F2_mhalf(1) = F(2);
    F3_mhalf(1) = F(3);
    
    % Build dQ/dt
   for i = 1:Nx
        dqdt(i,1) = -(F1_phalf(i) - F1_mhalf(i))/h;
        dqdt(Nx+i,1) = -(F2_phalf(i) - F2_mhalf(i))/h;
        dqdt(2*Nx+i,1) = -(F3_phalf(i) - F3_mhalf(i))/h;
   end
    
%%%%%%%%%%%%%%%%%%%%%%% Averaging Method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(sch, 'avg')    
    % Find F_(i+1/2)
    for i=1:Nx-1
        F1_phalf(i) = 0.5*(F1(i) + F1(i+1));
        F2_phalf(i) = 0.5*(F2(i) + F2(i+1));
        F3_phalf(i) = 0.5*(F3(i) + F3(i+1));
    end
    
    for i = 2:Nx
        F1_mhalf(i) = 0.5*(F1(i-1) + F1(i));
        F2_mhalf(i) = 0.5*(F2(i-1) + F2(i));
        F3_mhalf(i) = 0.5*(F3(i-1) + F3(i));
    end
    
    % Boundary Conditions - F_(i + 1/2) - NON periodic for shock tube
    % MODIFY
    F1_phalf(Nx) = 0.5*(F1(Nx) + F1(Nx));
    F2_phalf(Nx) = 0.5*(F2(Nx) + F2(Nx));
    F3_phalf(Nx) = 0.5*(F3(Nx) + F3(Nx));

    % Boundary Conditions - F_(i - 1/2) - NON periodic for shock tube
    % MODIFY
    F1_mhalf(1) = 0.5*(F1(1) + F1(1));
    F2_mhalf(1) = 0.5*(F2(1) + F2(1));
    F3_mhalf(1) = 0.5*(F3(1) + F3(1));
    
    % Build dQ/dt
   for i = 1:Nx
        dqdt(i,1) = -(F1_phalf(i) - F1_mhalf(i))/h ;
        dqdt(Nx+i,1) = -(F2_phalf(i) - F2_mhalf(i))/h;
        dqdt(2*Nx+i,1) = -(F3_phalf(i) - F3_mhalf(i))/h;
    end

    %%%%%%%%%%%%%%%%%%%%% Reimann Solver %%%%%%%%%%%%%%%%%%%%%%%% 
    
elseif strcmp(sch, 'Reimann')
    dqdt = reshape(dqdt, 3,Nx);
    Qr = reshape(Q,Nx,3);
    Qr = Qr';
    for i = 1:Nx-1
        F_phalf(:,i) = AppRiemannSol(Qr(:,i), Qr(:,i+1));
    end
        
    for i = 2:Nx
        F_mhalf(:,i) = AppRiemannSol(Qr(:,i-1), Qr(:,i));
    end
    % Boundary Conditions - F_(i+1/2), F_(i-1/2)
    F_mhalf(:,1) = AppRiemannSol(Qr(:,1), Qr(:,1));
    F_phalf(:,Nx) = AppRiemannSol(Qr(:,Nx), Qr(:,Nx));

    % Build dQ/dt
    for i = 1:Nx
        dqdt(:,i) = -(F_phalf(:,i) - F_mhalf(:,i))./h;
    end
    
    dqdt = dqdt';
    dqdt = reshape(dqdt, 3*Nx,1);
    
    
    end
end

