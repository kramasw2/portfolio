# README #

This repo contains sample code from various miscellaneous projects.

### What does this repo contain? ###

##Python:##

* Code from a Robot Path Optimization problem I helped create. It uses Gradient descent to implement obstacle avoidance along with path (length) optimization.
* Code from a Recommender systems problem I helped create. It basically uses a "rank-k" approximation on a sample dataset with movie ratings to recommend new movies to users, based on unknown data attributes.
* A few research scripts I used/modified to carry out Dynamic Mode Decomposition studies on PIV data I helped collect. The algorithm promotes sparsity in identifying dominant modes that define the evolution of the flow field via analyzing a sequence of snapshots.

##Matlab:##
* Code for a Shock Bubble Interaction Problem I wrote over the summer. The problem is currently set up to solve the 2D Euler Equations for the Shock Bubble interaction problem, although it can be used for other problems (preferably goverened by hyperbolic equations).
* The .m files included are functions used within the Main Shock_Bubble code.
* Roeflux.m is a roe flux computing function, that uses a Reimann solver instead of something like ODE45.

### How do I get set up? ###

The ipython notebooks can be run using an ipython, once downloaded and saved to the relevant directory. If you wish to see the code, the .py files are also included.
